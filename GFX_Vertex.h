// GFX_Vertex.h: interface for the GFX_Vertex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GFX_VERTEX_H__40D1C144_A277_421C_AFA6_B2EAF794DE4D__INCLUDED_)
#define AFX_GFX_VERTEX_H__40D1C144_A277_421C_AFA6_B2EAF794DE4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <glide.h>
#include <cmath>

#include "math3d.h"

class GFX_Vertex  
{
public:
	static void SetPosition(GrVertex *v, float x, float y)
	{
		v->x = x; 
		v->y = y;
	}

	static void SetPosition(GrVertex *v, float x, float y, float z)
	{
		v->x = x; 
		v->y = y;
		v->z = z;
	}

	static void SetColor(GrVertex *v, float r, float g, float b)
	{
		v->r = r;
		v->g = g;
		v->b = b;
	}

	static void SetColor(GrVertex *v, float r, float g, float b, float a)
	{
		v->r = r;
		v->g = g;
		v->b = b;
		v->a = a;
	}

	static void Rotate2D(GrVertex *v, const float degrees, const GrVertex *origin)
	{
		float radians = DEGtoRAD(degrees);
		float tempX, tempY;

		float originX = v->x - origin->x;
		float originY = v->y - origin->y;

		tempX = (originX * cosf(radians)) - (originY * sinf(radians));
		tempY = (originX * sinf(radians)) + (originY * cosf(radians));

		v->x = tempX + origin->x;
		v->y = tempY + origin->y;
	}

	GFX_Vertex();
	virtual ~GFX_Vertex();

};

#endif // !defined(AFX_GFX_VERTEX_H__40D1C144_A277_421C_AFA6_B2EAF794DE4D__INCLUDED_)
