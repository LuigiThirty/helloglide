// Vector3f.h: interface for the Vector3f class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VECTOR3F_H__D045F2C3_77F6_4D3C_9793_597BBC7BEABB__INCLUDED_)
#define AFX_VECTOR3F_H__D045F2C3_77F6_4D3C_9793_597BBC7BEABB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Vector3f  
{
public:
	float x, y, z;

	Vector3f() { x = y = z = 0; }
	Vector3f(float x, float y, float z);
	virtual ~Vector3f();

};

#endif // !defined(AFX_VECTOR3F_H__D045F2C3_77F6_4D3C_9793_597BBC7BEABB__INCLUDED_)
