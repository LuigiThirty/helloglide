// Matrix44.cpp: implementation of the Matrix class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Matrix.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Building translation/rotation/scale matrix functions.
Matrix44 Matrix44::Translation(Vector3f v)
{
	return Matrix44::Translation(v.x, v.y, v.z);
}

Matrix44 Matrix44::Translation(float x, float y, float z)
{
	Matrix44 m = Matrix44();
	m.Identity();

	m.data[3][0] = x;
	m.data[3][1] = y;
	m.data[3][2] = z;

	return m;
}

Matrix44 Matrix44::Scale(Vector3f v)
{
	return Matrix44::Scale(v.x, v.y, v.z);
}

Matrix44 Matrix44::Scale(float x, float y, float z)
{
	Matrix44 m = Matrix44();
	m.Identity();

	m.data[0][0] = x;
	m.data[1][1] = y;
	m.data[2][2] = z;

	return m;
}

// Roll
Matrix44 Matrix44::RotationX(float theta)
{
	Matrix44 m = Matrix44();
	m.Identity();

	m.data[1][1] = cosf(theta);
	m.data[1][2] = -sinf(theta);
	m.data[2][1] = sinf(theta);
	m.data[2][2] = cosf(theta);

	return m;
}

// Pitch
Matrix44 Matrix44::RotationY(float theta)
{
	Matrix44 m = Matrix44();
	m.Identity();

	m.data[0][0] = cosf(theta);
	m.data[0][2] = -sinf(theta);
	m.data[2][0] = sinf(theta);
	m.data[2][2] = cosf(theta);

	return m;
}

// Yaw
Matrix44 Matrix44::RotationZ(float theta)
{
	Matrix44 m = Matrix44();
	m.Identity();

	m.data[0][0] = cosf(theta);
	m.data[1][0] = sinf(theta);
	m.data[0][1] = -sinf(theta);
	m.data[1][1] = cosf(theta);

	return m;
}

Matrix44 Matrix44::Rotation(Vector3f v)
{
	return Matrix44::Rotation(v.x, v.y, v.z);
}

Matrix44 Matrix44::Rotation(float yaw, float pitch, float roll)
{
#define ALPHA yaw
#define BETA pitch
#define GAMMA roll

	yaw = DEGtoRAD(yaw);
	pitch = DEGtoRAD(pitch);
	roll = DEGtoRAD(roll);

	// Compose a rotation matrix from Z, Y, and X rotations.
	Matrix44 m = Matrix44();
	m.Identity();

	m.data[0][0] = cosf(ALPHA) * cosf(BETA);
	m.data[0][1] = (cosf(ALPHA) * sinf(BETA) * sinf(GAMMA)) - (sinf(ALPHA) * cosf(GAMMA));
	m.data[0][2] = (cosf(ALPHA) * sinf(BETA) * cosf(GAMMA)) + (sinf(ALPHA) * sinf(GAMMA));

	m.data[1][0] = sinf(ALPHA) * cosf(BETA);
	m.data[1][1] = (sinf(ALPHA) * sinf(BETA) * sinf(GAMMA)) + (cosf(ALPHA) * cosf(GAMMA));
	m.data[1][2] = (sinf(ALPHA) * sinf(BETA) * cosf(GAMMA)) - (cosf(ALPHA) * sinf(GAMMA));

	m.data[2][0] = -sinf(BETA);
	m.data[2][1] = cosf(BETA) * sinf(GAMMA);
	m.data[2][2] = cosf(BETA) * cosf(GAMMA);

#undef ALPHA
#undef BETA
#undef GAMMA

	return m;
}