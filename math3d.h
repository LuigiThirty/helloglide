#pragma once

#include "Vector3f.h"

#define M_PI 3.14159265358979323846

const int screen_width  = 640;
const int screen_height = 480;

inline float DEGtoRAD(float x)
{
	return (float)(x * (M_PI / 180.0));
}