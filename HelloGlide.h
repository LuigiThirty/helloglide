#pragma once

#include <windows.h>
#include <conio.h>
#include <stdlib.h>
#include <cstring>
#include <glide.h>
#include <cmath>

#include "GFX_Vertex.h"
#include "math3d.h"
#include "Matrix.h"

GrHwConfiguration hwconfig;

void GlideLoop();
