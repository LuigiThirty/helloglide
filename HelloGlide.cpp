// HelloGlide.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "HelloGlide.h"
#include "Math3D.h"
#include "Model3D.h"

#include <vector>

Model3D *squareModel;
Model3D *squareModel2;

GrVertex square[16];

const float viewAngle = 90;
const float clipNear = 0.1;
const float clipFar = 100.0;

GrVertex projectedVertex[100];

Matrix44 mWorld, mProjection;

GrVertex * transformedVertexes;

int main(int argc, char* argv[])
{	
	bool glideActivated = false;

	mProjection = Matrix44();;
	float scale = 1.0 / (float)(tanf((viewAngle * 0.5) * (M_PI / 180.0)));
	mProjection.data[0][0] = scale;
	mProjection.data[1][1] = scale;
	mProjection.data[2][2] = -(clipFar / (clipFar-clipNear));
	mProjection.data[3][2] = -((clipFar * clipNear) / (clipFar-clipNear));
	mProjection.data[2][3] = -1; // w = -z
	mProjection.data[3][3] = 0;

	mWorld = Matrix44();
	mWorld.Identity();
	mWorld.data[3][2] = -10;

	GFX_Vertex::SetPosition(&square[0], -1, -1,	1);
	GFX_Vertex::SetPosition(&square[1], 1,	-1,	1);
	GFX_Vertex::SetPosition(&square[2], 1,	-1, -1);
	GFX_Vertex::SetPosition(&square[3], -1, -1, -1);

	GFX_Vertex::SetPosition(&square[4], -1,	1,	1);
	GFX_Vertex::SetPosition(&square[5], 1,	1,	1);
	GFX_Vertex::SetPosition(&square[6], 1,	1, -1);
	GFX_Vertex::SetPosition(&square[7], -1,	1, -1);

	GFX_Vertex::SetColor(&square[0], 200,	0,		0,		0.5);
	GFX_Vertex::SetColor(&square[1], 0,		200,	0,		0.5);
	GFX_Vertex::SetColor(&square[2], 0,		0,		200,	0.5);
	GFX_Vertex::SetColor(&square[3], 100,	100,	100,	0.5);
	GFX_Vertex::SetColor(&square[4], 200,	0,		0,		0.5);
	GFX_Vertex::SetColor(&square[5], 0,		200,	0,		0.5);
	GFX_Vertex::SetColor(&square[6], 0,		0,		200,	0.5);
	GFX_Vertex::SetColor(&square[7], 100,	100,	100,	0.5);

	squareModel				= new Model3D(square, 8);
	squareModel->position	= Vector3f(0, 0, 0);
	squareModel->rotation	= Vector3f(0, 0, 0);
	squareModel->scale		= Vector3f(1, 1, 1);

	squareModel2			= new Model3D(square, 8);
	squareModel2->position	= Vector3f(0, 0, 0);
	squareModel2->rotation	= Vector3f(0, 0, 0);
	squareModel2->scale		= Vector3f(1, 1, 1);

	int faces[6][4] = { {3,2,1,0}, {1,5,4,0}, {2,6,5,1}, {7,6,2,3}, {4,7,3,0}, {5,6,7,4} };
	for(int i=0; i<6; i++)
	{
		squareModel->AddFace(new ModelFace(4, faces[i]));
		squareModel2->AddFace(new ModelFace(4, faces[i]));
	}

	grGlideInit();
	if(grSstQueryHardware(&hwconfig))
	{
		grSstSelect(0);
		printf("Selected Voodoo 0\n");
		grSstWinOpen(NULL, GR_RESOLUTION_640x480, GR_REFRESH_60Hz,
			GR_COLORFORMAT_ARGB, GR_ORIGIN_LOWER_LEFT, 2, 0);

		GlideLoop();
	}
	else
	{
		printf("No Voodoo found?\n");
	}

	if(glideActivated)
	{
		grGlideShutdown();
	}

	return 0;
}

void GlideLoop()
{
	GrVertex p[1000];

	// 1000 random points
	for(int n=0; n<1000; n++)
	{
		p[n].x = (float)(rand() % 1024);
		p[n].y = (float)(rand() % 1024);
		p[n].z = 0;
	}

	GrVertex center;
	center.x = screen_width/2;
	center.y = screen_height/2;

	float degrees = 0;
	float newZ = 0;

	float newX = -10;

	Matrix44 movement = Matrix44();
	movement.Identity();

	transformedVertexes = (GrVertex *)malloc(sizeof(GrVertex *) * 10);

	//grDepthMask(FXTRUE);
	//grDepthBufferMode(GR_DEPTHBUFFER_ZBUFFER);
	//grDepthBufferFunction(GR_CMP_LESS);

	grCullMode(GR_CULL_NEGATIVE);

	bool exit = false;
	while(exit == false)
	{
		degrees += 0.25;

		grBufferClear(0, 0, GR_ZDEPTHVALUE_FARTHEST);

		grColorCombine(	GR_COMBINE_FUNCTION_LOCAL, GR_COMBINE_FACTOR_NONE,
						GR_COMBINE_LOCAL_CONSTANT, GR_COMBINE_OTHER_NONE, FXFALSE );

		for(int i=0; i<1000; i++)
		{
			grDrawPoint(&(p[i]));
		}

		// Draw a gouraud-shaded polygon.
		grColorCombine(	GR_COMBINE_FUNCTION_LOCAL, GR_COMBINE_FACTOR_NONE,
						GR_COMBINE_LOCAL_ITERATED, GR_COMBINE_OTHER_NONE, FXFALSE );
		
		newZ = (newZ + 0.25);
		if(newZ > 360.0) newZ -= 360.0;

		newX += 0.025;
		if(newX > 10) newX -= 20;

		squareModel->rotation = Vector3f(newZ, newZ, newZ);
		{
			ModelFace *head = squareModel->GetFaceList();
			ModelFace *current = head;

			while(current != NULL)
			{
				squareModel->GetTransformedFace(transformedVertexes, 
					current,
					&mWorld,
					&mProjection);
				grDrawPolygonVertexList(current->vertexCount, transformedVertexes);

				current = current->next;
			}
		}

		squareModel2->position = Vector3f(newX, 2, -1);
		{
			ModelFace *head = squareModel2->GetFaceList();
			ModelFace *current = head;

			while(current != NULL)
			{
				squareModel2->GetTransformedFace(transformedVertexes, 
					current,
					&mWorld,
					&mProjection);
				grDrawPolygonVertexList(current->vertexCount, transformedVertexes);

				current = current->next;
			}
		}

		// 60/N fps
		grBufferSwap(2);

		if(kbhit() > 0)
		{
			exit = true;
		}

	}
}