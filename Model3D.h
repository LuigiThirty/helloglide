// Model3D.h: interface for the Model3D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODEL3D_H__21C804C4_84B4_4D1C_91B1_81324BEE5383__INCLUDED_)
#define AFX_MODEL3D_H__21C804C4_84B4_4D1C_91B1_81324BEE5383__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <glide.h>
#include <vector>

#include "GFX_Vertex.h"
#include "math3d.h"
#include "Matrix.h"

typedef struct model_face_t
{
	// Link to next ModelFace
	struct model_face_t *next;

	int vertexCount;
	int *vertexIndexes;

	model_face_t() {};

	model_face_t(int count, int indexes[])
	{
		next = NULL;
		vertexCount = count;
		vertexIndexes = NULL;

		vertexIndexes = (int *)malloc(sizeof(int) * count);
		memcpy(vertexIndexes, indexes, sizeof(int)*count);
	}

	~model_face_t()
	{
		//if(vertexIndexes != NULL)
			//free(vertexIndexes);
	}

} ModelFace;

class Model3D  
{
	GrVertex *vertexList;

	int facesCount;
	ModelFace *faces;

	int vertexCount;
	Matrix44 myTransform;

public:
	Vector3f position, rotation, scale;

	void AddFace(ModelFace *face);

	ModelFace *GetFaceList() { return faces; }
	int GetFaceCount() { return facesCount; }
	//int GetVertexCountForFace(int index) { return faces.at(index).vertexCount; }

	GrVertex * GetVertexesForFace(ModelFace *face);
	void GetTransformedFace(GrVertex *, ModelFace *face, Matrix44 *world, Matrix44 *projection);

	void SetModelTransform(Matrix44 *transform) 
		{ memcpy(&myTransform, transform, sizeof(Matrix44)); }
	
	Matrix44 GetModelTransform() { return myTransform; }


	Model3D(GrVertex *vertexList, int vertexCount)
	{
		this->faces = NULL;

		this->scale = Vector3f(1,1,1);

		this->vertexList = vertexList;
		this->vertexCount = vertexCount;

		this->myTransform.Identity();
	}
	~Model3D() {};

};

#endif // !defined(AFX_MODEL3D_H__21C804C4_84B4_4D1C_91B1_81324BEE5383__INCLUDED_)
