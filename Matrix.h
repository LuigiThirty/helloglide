// Matrix44.h: interface for the Matrix44 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_Matrix44_H__513CD7B8_3453_416D_888A_B818B5C673BA__INCLUDED_)
#define AFX_Matrix44_H__513CD7B8_3453_416D_888A_B818B5C673BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <glide.h>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include "Math3D.h"

class Matrix44  
{
private:
	int rowCount;
	int colCount;

public:
	float data[4][4];

	Matrix44()
	{
		rowCount = 4;
		colCount = 4;

		for(int i=0; i<rowCount; i++)
		{
			for(int j=0; j<colCount; j++)
			{
				data[i][j] = 0;
			}
		}
	}

	void Identity()
	{
		data[0][0] = 1;	data[0][1] = 0;	data[0][2] = 0;	data[0][3] = 0;
		data[1][1] = 0;	data[1][1] = 1;	data[1][2] = 0;	data[1][3] = 0;
		data[2][2] = 0;	data[2][1] = 0;	data[2][2] = 1;	data[2][3] = 0;
		data[3][3] = 0;	data[3][1] = 0;	data[3][2] = 0;	data[3][3] = 1;
	}

	void TransformPoint(const GrVertex *in, GrVertex *out)
	{
		//memcpy(out, in, sizeof(GrVertex));

		out->x = (in->x * data[0][0]) + (in->y * data[1][0]) + (in->z * data[2][0]) + data[3][0];
		out->y = (in->x * data[0][1]) + (in->y * data[1][1]) + (in->z * data[2][1]) + data[3][1];
		out->z = (in->x * data[0][2]) + (in->y * data[1][2]) + (in->z * data[2][2]) + data[3][2];
		float w =(in->x * data[0][3]) + (in->y * data[1][3]) + (in->z * data[2][3]) + data[3][3];

		if(w != 1)
		{
			out->x /= w;
			out->y /= w;
			out->z /= w;
		}

		out->r = in->r;
		out->g = in->g;
		out->b = in->b;
		out->a = in->a;
	}

	~Matrix44() {}

	static Matrix44 Translation(Vector3f v);
	static Matrix44 Translation(float x, float y, float z);

	static Matrix44 Scale(Vector3f v);
	static Matrix44 Scale(float x, float y, float z);

	static Matrix44 Rotation(Vector3f v);
	static Matrix44 Rotation(float yaw, float pitch, float roll);
	
	static Matrix44 RotationX(float theta);
	static Matrix44 RotationY(float theta);
	static Matrix44 RotationZ(float theta);

	Matrix44 Multiply(Matrix44 *b)
	{
		Matrix44 result = Matrix44();

		/*
		for(int row=0; row<4; row++)
		{
			for(int col=0; col<4; col++)
			{
				result.data[row][col] = 0;

				for(int n=0; n<4; n++)
				{
					result.data[row][col] += this->data[col][n] * b->data[n][row];
				}
			}
		}
		*/

		for(int rightRow=0; rightRow<4; rightRow++)
		{
			for(int r=0; r<4; r++)
			{
				for(int c=0; c<4; c++)
				{
					result.data[r][rightRow] += this->data[r][c] * b->data[c][rightRow];
				}
			}
		}

		return result;
	}
};

#endif // !defined(AFX_MATRIX_H__513CD7B8_3453_416D_888A_B818B5C673BA__INCLUDED_)
