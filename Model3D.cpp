// Model3D.cpp: implementation of the Model3D class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Model3D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void Model3D::AddFace(ModelFace *face)
{
	// Supports polygons with any number of vertices.
	// The Voodoo will automatically decompose them into triangles.
	//faces.push_back(face);

	ModelFace *head = faces;
	ModelFace *current = head;

	if(head == NULL)
		faces = face;
	else
	{
		ModelFace *previous;

		while(current != NULL)
		{
			previous = current;
			current = current->next;
		}

		current = face;
		previous->next = face;
	}

	facesCount++;
}	

GrVertex * Model3D::GetVertexesForFace(ModelFace *face)
{
	// Returns a GrVertex array containing the GrVertex pointers
	// for face N.

	GrVertex *vertexes = new GrVertex[face->vertexCount];

	for(int v=0; v<face->vertexCount; v++)
	{
		vertexes[v] = this->vertexList[face->vertexIndexes[v]];
	}

	return vertexes;
}

void Model3D::GetTransformedFace(GrVertex *output, ModelFace *face, Matrix44 *world, Matrix44 *projection)
{		
	Matrix44 transformation = Matrix44();
	transformation.Identity();

	// Construct a transform from our model's transform.
	Matrix44 model = Matrix44();
	model.Identity();

	Matrix44 mScale = Matrix44::Scale(this->scale);
	Matrix44 mRotation = Matrix44::Rotation(this->rotation);
	Matrix44 mTranslation = Matrix44::Translation(this->position);

	Matrix44 mRotX = Matrix44::RotationX(DEGtoRAD(this->rotation.x));
	Matrix44 mRotY = Matrix44::RotationY(DEGtoRAD(this->rotation.y));
	Matrix44 mRotZ = Matrix44::RotationZ(DEGtoRAD(this->rotation.z));

	model = model.Multiply(&mScale);
	model = model.Multiply(&mRotation);
	model = model.Multiply(&mTranslation);

	GrVertex *faceVertexes = GetVertexesForFace(face);

	transformation = transformation.Multiply(&model);
	transformation = transformation.Multiply(world);	
	transformation = transformation.Multiply(projection);

	for(int v=0; v < face->vertexCount; v++)
	{
		transformation.TransformPoint(&faceVertexes[v], &output[v]);
		//printf("transformation: vertex %d == %f %f %f\n", 
		//	v, output[v].x, output[v].y, output[v].z);
		
		// Transform to the Glide coordinate space.
		output[v].x = (output[v].x * screen_width) + (screen_width / 2);
		output[v].y = (output[v].y * screen_height) + (screen_height / 2);
	}

	//delete faceVertexes;
}
